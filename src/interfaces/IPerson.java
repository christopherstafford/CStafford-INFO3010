/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import dao.Person;

/**
 *
 * @author chris
 */
public interface IPerson {
    
    String getName();
    void setName(String name);
    
    String getAddress();
    void setAddress(String address);
    
    String getSocialSecurityNumber();
    void setSocialSecurityNumber(String dateOfBirth);
    
    long getDateOfBirth();
    void setDateOfBirth(long dateOfBirth);
    
}
