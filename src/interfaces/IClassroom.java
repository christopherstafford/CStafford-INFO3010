/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

import enums.RoomType;

/**
 *
 * @author chris
 */
public interface IClassroom {
    
    String getRoomNumber();
    void setRoomNumber(String roomnumber);
    
    RoomType getTypeOfRoom();
    void setTypeOfRoom(RoomType roomtype);
    
}
