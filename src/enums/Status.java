/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enums;

import java.util.HashMap;

/**
 *
 * @author chris
 */
public enum Status {
    
    PARTTIME('P'),
    FULLTIME('F'),
    TERMINATED('T');

    HashMap<Character, String> statusMap = new HashMap<Character, String>() {
        {
            put('P', "Part Time");
            put('F', "Full Time");
            put('T', "Terminated");
        }
    };

    private final char status;

    Status(char status) {
        this.status = status;
    }

    public int getStatus() {
        return this.status;
    }
    
    public String getStatusString() {
        return statusMap.get(this.status);
    }
    
}
