package enums;

import java.util.HashMap;

public enum RoomType {
    
    LAB('L'),
    CLASSROOM('C'),
    LECTUREHALL('H');

    HashMap<Character, String> roomTypeMap = new HashMap<Character, String>() {
        {
            put('L', "Laboratory");
            put('C', "Classroom");
            put('H', "Lecture Hall");
        }
    };

    private final char roomType;

    RoomType(char type) {
        this.roomType = type;
    }

    public int getRoomType() {
        return this.roomType;
    }
    
    public String getRoomTypeString() {
        return roomTypeMap.get(this.roomType);
    }
    
}
