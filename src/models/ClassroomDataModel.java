/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import dao.Classroom;
import java.util.ArrayList;
/**
 *
 * @author chris
 */
public class ClassroomDataModel {
    
    private ArrayList<Classroom> listOfClassrooms;

    public ClassroomDataModel() {
        listOfClassrooms = new ArrayList<>();
    }
    
    public ArrayList<Classroom> getListOfClassrooms() {
        return listOfClassrooms;
    }
    
    public void setListOfClassrooms(ArrayList<Classroom> listOfClassrooms) {
        this.listOfClassrooms = listOfClassrooms;
    }
    
}
