/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import dao.Faculty;

import java.util.ArrayList;
/**
 *
 * @author chris
 */
public class FacultyDataModel {
    
    private ArrayList<Faculty> listOfFaculty;

    public FacultyDataModel() {
        listOfFaculty = new ArrayList<>();
    }
    
    

    public ArrayList<Faculty> getListOfFaculty() {
        return listOfFaculty;
    }

    public void setListOfFaculty(ArrayList<Faculty> listOfFaculty) {
        this.listOfFaculty = listOfFaculty;
    }
    
    
    
}
