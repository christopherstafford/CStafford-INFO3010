/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import dao.Student;

import java.util.ArrayList;

/**
 *
 * @author chris
 */
public class StudentDataModel {
    
    private ArrayList<Student> listOfStudents;

    public StudentDataModel() {
        listOfStudents = new ArrayList<>();
    }

    public ArrayList<Student> getListOfStudents() {
        return listOfStudents;
    }

    public void setListOfStudents(ArrayList<Student> listOfStudents) {
        this.listOfStudents = listOfStudents;
    }
    
    
    
    
    
}
