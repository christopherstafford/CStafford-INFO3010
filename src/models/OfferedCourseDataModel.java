/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import dao.OfferedCourse;

import java.util.ArrayList;

/**
 *
 * @author chris
 */
public class OfferedCourseDataModel {
    
    private ArrayList<OfferedCourse> listOfOfferedCourses;

    public OfferedCourseDataModel() {
        listOfOfferedCourses = new ArrayList<>();
    }

    public ArrayList<OfferedCourse> getListOfOfferedCourses() {
        return listOfOfferedCourses;
    }

    public void setListOfOfferedCourses(ArrayList<OfferedCourse> listOfOfferedCourses) {
        this.listOfOfferedCourses = listOfOfferedCourses;
    }
    
    
    
    
}
