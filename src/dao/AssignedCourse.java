/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author chris
 */
public class AssignedCourse {
    
    private String courseId;

    public AssignedCourse() {
    }

    public AssignedCourse(String courseId) {
        this.courseId = courseId;
    }
    
    

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    @Override
    public String toString() {
        return "AssignedCourse{" + "courseId=" + courseId + '}';
    }
    
    
    
}
