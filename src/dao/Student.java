/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.ArrayList;

/**
 *
 * @author chris
 */
public class Student extends Person {
    
    private float currentGPA;
    private long dateOfGraduation;
    private ArrayList<AssignedCourse> enrolledCourses = new ArrayList<>();

    public Student() {
    }


    public Student(float currentGPA, long dateOfGraduation, ArrayList<AssignedCourse> enrolledCourses, String name, String address, String socialSecurityNumber, long dateOfBirth) {
        super(name, address, socialSecurityNumber, dateOfBirth);
        this.currentGPA = currentGPA;
        this.dateOfGraduation = dateOfGraduation;
        this.enrolledCourses = enrolledCourses;
    }

    

    public float getCurrentGPA() {
        return currentGPA;
    }

    public void setCurrentGPA(float currentGPA) {
        this.currentGPA = currentGPA;
    }

    public long getDateOfGraduation() {
        return dateOfGraduation;
    }

    public void setDateOfGraduation(long dateOfGraduation) {
        this.dateOfGraduation = dateOfGraduation;
    }

    public ArrayList<AssignedCourse> getEnrolledCourses() {
        return enrolledCourses;
    }

    public void setEnrolledCourses(ArrayList<AssignedCourse> enrolledCourses) {
        this.enrolledCourses = enrolledCourses;
    }



    @Override
    public String toString() {
        return "Student{" + "currentGPA=" + currentGPA + ", dateOfGraduation=" + dateOfGraduation + ", enrolledCourses=" + enrolledCourses + '}';
    }

    
    
    
}
