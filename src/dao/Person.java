/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;
import interfaces.IPerson;

/**
 *
 * @author chris
 */
public class Person {
    
    private String name;
    private String address;
    private String socialSecurityNumber;
    private long dateOfBirth;

    public Person() {
    }

    public Person(String name, String address, String socialSecurityNumber, long dateOfBirth) {
        this.name = name;
        this.address = address;
        this.socialSecurityNumber = socialSecurityNumber;
        this.dateOfBirth = dateOfBirth;
    }
    
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public long getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(long dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public String toString() {
        return "Person{" + "name=" + name + ", address=" + address + ", socialSecurityNumber=" + socialSecurityNumber + ", dateOfBirth=" + dateOfBirth + '}';
    }
    
    
    
}
