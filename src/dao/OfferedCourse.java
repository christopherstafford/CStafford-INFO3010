/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

/**
 *
 * @author chris
 */
public class OfferedCourse {
    
    private String courseId;
    private String courseName;
    private Classroom classrom;

    public OfferedCourse() {
    }

    public OfferedCourse(String courseId, String courseName, Classroom classrom) {
        this.courseId = courseId;
        this.courseName = courseName;
        this.classrom = classrom;
    }
    
    

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public Classroom getClassrom() {
        return classrom;
    }

    public void setClassrom(Classroom classrom) {
        this.classrom = classrom;
    }

    @Override
    public String toString() {
        return "OfferedCourse{" + "courseId=" + courseId + ", courseName=" + courseName + ", classrom=" + classrom + '}';
    }
    
    
    
}
