/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import enums.RoomType;
import interfaces.IClassroom;

/**
 *
 * @author chris
 */
public class Classroom {
    
    private String roomNumber;
    private RoomType typeOfRoom;

    public Classroom() {
    }

    public Classroom(String roomNumber, RoomType typeOfRoom) {
        this.roomNumber = roomNumber;
        this.typeOfRoom = typeOfRoom;
    }
    
    
    
    

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public RoomType getTypeOfRoom() {
        return typeOfRoom;
    }

    public void setTypeOfRoom(RoomType typeOfRoom) {
        this.typeOfRoom = typeOfRoom;
    }

    @Override
    public String toString() {
        return "Classroom{" + "roomNumber=" + roomNumber + ", typeOfRoom=" + typeOfRoom + '}';
    }


    
    
    
}
