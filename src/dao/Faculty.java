/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import enums.Status;
import java.util.ArrayList;

/**
 *
 * @author chris
 */
public class Faculty extends Person {
    private long dateOfHire;
    private long dateOfTermination;
    private double salary;
    private Status status;
    private ArrayList<AssignedCourse> listOfCourses = new ArrayList<>();

    public Faculty() {
    }


    public Faculty(long dateOfHire, long dateOfTermination, double salary, Status status, ArrayList<AssignedCourse> listOfCourses, String name, String address, String socialSecurityNumber, long dateOfBirth) {
        super(name, address, socialSecurityNumber, dateOfBirth);
        this.dateOfHire = dateOfHire;
        this.dateOfTermination = dateOfTermination;
        this.salary = salary;
        this.status = status;
        this.listOfCourses = listOfCourses;
    }


    
    
    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ArrayList<AssignedCourse> getListOfCourses() {
        return listOfCourses;
    }

    public void setListOfCourses(ArrayList<AssignedCourse> listOfCourses) {
        this.listOfCourses = listOfCourses;
    }

    

    public long getDateOfHire() {
        return dateOfHire;
    }

    public void setDateOfHire(long dateOfHire) {
        this.dateOfHire = dateOfHire;
    }

    public long getDateOfTermination() {
        return dateOfTermination;
    }

    public void setDateOfTermination(long dateOfTermination) {
        this.dateOfTermination = dateOfTermination;
    }

    @Override
    public String toString() {
        return "Faculty{" + "dateOfHire=" + dateOfHire + ", dateOfTermination=" + dateOfTermination + ", salary=" + salary + ", status=" + status + ", listOfCourses=" + listOfCourses + '}';
    }


    
    
}
