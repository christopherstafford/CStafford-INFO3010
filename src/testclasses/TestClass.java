
import dao.AssignedCourse;
import dao.Classroom;
import dao.Faculty;
import dao.OfferedCourse;
import dao.Student;
import enums.RoomType;
import enums.Status;
import java.util.Date;
import models.ClassroomDataModel;
import models.FacultyDataModel;
import models.OfferedCourseDataModel;
import models.StudentDataModel;

public class TestClass {

    public static void main(String[] args) {

        // Create a date which will be used in all test methods
        Date d = new Date();
        long testDate = d.getTime();

        // Create an AssignedCourse object used in faculty and students
        AssignedCourse assignedCourse = new AssignedCourse("INFO301");

        // Create a classroom data model
        ClassroomDataModel cdm = new ClassroomDataModel();

        // Create classrooms and store in data model
        Classroom classroom1 = new Classroom("PA100", RoomType.CLASSROOM);
        cdm.getListOfClassrooms().add(classroom1);
        Classroom classroom2 = new Classroom("FA100", RoomType.LAB);
        cdm.getListOfClassrooms().add(classroom2);

        // Print the data in the data model using the for-each looping construct
        for (Classroom oneroom : cdm.getListOfClassrooms()) {
            System.out.println(oneroom.toString());
        }

        // Create a faculty1 data model
        FacultyDataModel fdm = new FacultyDataModel();

        // Create several Faculty and store in data model
        Faculty faculty1 = new Faculty();
        faculty1.setDateOfBirth(testDate);
        faculty1.setName("Elizabeth Kramer");
        faculty1.setAddress("1 University Ave");
        faculty1.setSocialSecurityNumber("123456789");
        faculty1.setStatus(Status.PARTTIME);
        faculty1.setDateOfHire(testDate);
        faculty1.setDateOfTermination(testDate);
        faculty1.setSalary(500000);
        faculty1.getListOfCourses().add(assignedCourse);
        fdm.getListOfFaculty().add(faculty1);

        Faculty faculty2 = new Faculty();
        faculty2.setDateOfBirth(testDate);
        faculty2.setName("John Smith");
        faculty2.setAddress("1 University Ave");
        faculty2.setSocialSecurityNumber("987654321");
        faculty2.setStatus(Status.FULLTIME);
        faculty2.setDateOfHire(testDate);
        faculty2.setDateOfTermination(testDate);
        faculty2.setSalary(500000);
        faculty2.getListOfCourses().add(assignedCourse);
        fdm.getListOfFaculty().add(faculty2);

        // Print the data in the data model using the for-each looping construct
        for (Faculty faculty : fdm.getListOfFaculty()) {
            System.out.println(faculty.toString());
        }

        // Create a student data model
        StudentDataModel sdm = new StudentDataModel();

        // Create several students and store in data model 
        Student student1 = new Student();
        student1.setDateOfBirth(testDate);
        student1.setName("Kathy Smith");
        student1.setAddress("1 Beech Ave");
        student1.setSocialSecurityNumber("123456789");
        student1.setDateOfGraduation(testDate);
        student1.setCurrentGPA(0.0f);
        student1.getEnrolledCourses().add(assignedCourse);
        sdm.getListOfStudents().add(student1);

        Student student2 = new Student();
        student2.setDateOfBirth(testDate);
        student2.setName("Billy Bob");
        student2.setAddress("1 First Ave");
        student2.setSocialSecurityNumber("987654321");
        student2.setDateOfGraduation(testDate);
        student2.setCurrentGPA(0.0f);
        student2.getEnrolledCourses().add(assignedCourse);
        sdm.getListOfStudents().add(student2);

        // Print the data in the data model using the for-each looping construct
        for (Student student : sdm.getListOfStudents()) {
            System.out.println(student.toString());
        }
        
        // Create a data model for offered courses
        OfferedCourseDataModel ocdm = new OfferedCourseDataModel();
        
        // Create several offered course objects and store in data model
        OfferedCourse course1 = new OfferedCourse("INFO301", "Java Programming", classroom1);
        ocdm.getListOfOfferedCourses().add(course1);
        OfferedCourse course2 = new OfferedCourse("INFO303", "Advanced Java Programming", classroom2);
        ocdm.getListOfOfferedCourses().add(course2);

         // Print the data in the data model using the for-each looping construct
        for (OfferedCourse course : ocdm.getListOfOfferedCourses()) {
            System.out.println(course.toString());
        }
        
        
    }
}